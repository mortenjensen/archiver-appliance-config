# epicsarchiver-config

This repository is used to store all PVs to archive for archivers on technical network (and lab network, for now)


## Introduction

For each IOC, the list of PVs to archive should be saved in a file named `<ioc name>.archive`.
The file should be put under a directory named after the archiver appliance fully-qualified domain name.

Here is the list of current archiver appliances:

- **archiver-01.tn.esss.lu.se** for the FEB and ACC systems, Cryo, ODH and TS2
- **archiver-02.tn.esss.lu.se** for the FEB and ACC systems, Cryo, ODH and TS2
- **archiver-03.tn.esss.lu.se** for BI and RF waveform
- **archiver-04.tn.esss.lu.se** for BI and RF waveform
- **ics-archiver11.tn.esss.lu.se** for testing
- **nas-beam-01.cslab.esss.lu.se** for Beam Diagnostic test

archiver-01 and archiver-02 are part of the same cluster. So all PVs can be accessed from both appliances. They have the same EPICS_CA_ADDR_LIST.

archiver-03 and archiver-04 are part of the same cluster. So all PVs can be accessed from both appliances. They have the same EPICS_CA_ADDR_LIST.

**ALL WaveForm have to be archived at 1Hz** (see Archive File Format below).

## Archive File format

The files shall be in CSV format (space separated) and include one PV name per line.
A file can also include the name of the policy to force (optional).
Empty lines and lines starting with "#" (comments) are allowed.

Here is an example:

```
# PV name    Policy
ISrc-010:PwrC-CoilPS-01:CurS
ISrc-010:PwrC-CoilPS-01:CurR    1Hz
# Comments are allowed
LEBT-010:Vac-VCG-30000:PrsStatR
```

The string after the PV name should be an existing policy to force.
In the above example, the policy "1Hz" would be applied to the PV "ISrc-010:PwrC-CoilPS-01:CurR".
The default policy would be applied to other PVs.

## Workflow

When pushing to master, the PVs are automatically added to the archiver.

The `process_archives.py` script looks at files that changed since last commit.
All the PVs from those files are sent to the proper appliance for archiving.

PV deletion is currently not managed by the scripts.
